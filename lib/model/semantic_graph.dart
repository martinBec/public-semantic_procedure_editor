import 'dart:convert';

class SemanticGraph {
  static const String  GRAPH =  "@graph";
  static const String  CONTEXT =  "@context";

  final List<dynamic> statements;
  final Map<String, dynamic> context;

  const SemanticGraph({
    required this.statements,
    required this.context
  });

  factory SemanticGraph.fromString(jsonString) {
    SemanticGraph graph;
    Map<String, dynamic> graphJson = jsonDecode(jsonString);

    if (graphJson.containsKey(GRAPH)) {
      graph = SemanticGraph.fromGraphJson(graphJson);
    }
    else if (graphJson.containsKey(CONTEXT)) {
      graph = SemanticGraph.fromJson(graphJson);
    }
    else {
      graph = SemanticGraph(statements: [], context: {});
    }

    return graph;
  }

  factory SemanticGraph.fromJson(Map<String, dynamic> json) {

    Map<String, dynamic> context = json.remove(CONTEXT);

    return SemanticGraph(
        statements: [json],
        context: context
    );
  }

  factory SemanticGraph.fromGraphJson(Map<String, dynamic> json) {
    return SemanticGraph(
        statements: json[GRAPH],
        context: json[CONTEXT]
    );
  }
}