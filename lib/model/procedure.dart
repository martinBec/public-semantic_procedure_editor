import 'package:semantic_procedure_editor/model/semantic_graph.dart';
import 'package:semantic_procedure_editor/classes/proxy_step_list.dart';
import 'package:semantic_procedure_editor/model/step.dart';

import 'ontology_terms.dart';

class Procedure {

  static const String  PROCEDURE_TITLE =  "ars:hasProcedureTitle";
  static const String  PROCEDURE_DESCRIPTION =  "ars:hasProcedureDescription";

  String id = "";
  String title = "";
  String description = "";
  Step? firstStep;
  ProxyStepList steps = ProxyStepList();

  Procedure.draft();

  Procedure.listItem(Map<String, dynamic> procedureJson) {
    this.id = procedureJson[RDFTerms.ID] as String;
    this.title = procedureJson[Procedure.PROCEDURE_TITLE] as String;
  }

   Procedure.detail(SemanticGraph graph) {

     graph.statements.forEach((element) {
       Map<String, dynamic> entity = element as Map<String,dynamic>;
       String type = entity[RDFTerms.TYPE];

       switch (type) {

         case OntologyTerms.PROCEDURE:
           this.id = entity[RDFTerms.ID];
           this.title = entity[PROCEDURE_TITLE];
           this.description = entity[PROCEDURE_DESCRIPTION];
           break;

         case OntologyTerms.STEP:
           this.firstStep = Step(entity);
           break;
       }
     });
  }

  Map toJson() => {
    "title": this.title,
    "description": this.description
  };

  Map toEditionJson() => {
    "id": this.id,
    "title": this.title,
    "description": this.description,
    "first_step": this.steps.first.id
  };
}
