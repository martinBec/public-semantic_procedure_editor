import 'package:semantic_procedure_editor/model/ontology_terms.dart';
import 'package:uuid/uuid.dart';

class Step {

   static const String STEP_TITLE = "ars:hasStepTitle";
   static const String STEP_DESCRIPTION = "ars:hasStepDescription";
   static const String STEP_IMAGE = "ars:hasImageURL";
   static const String NEXT_STEP = "ars:nextStep";
   static const String PREVIOUS_STEP = "ars:previousStep";

   String id = "";
   String title = "";
   String imageUrl = "";
   String description = "";
   String nextStep = "";
   String previousStep = "";
   bool   modified = false;

   Step.draft() {
     var uuid = Uuid();
     this.id = "ars:" + uuid.v1();
   }

   Step(Map<String, dynamic> stepJson) {
     this.id = stepJson[RDFTerms.ID];
     this.title = stepJson[STEP_TITLE] ?? "";
     this.description = stepJson[STEP_DESCRIPTION] ?? "";
     this.imageUrl = "";

     if (stepJson.containsKey(NEXT_STEP)) {
       this.nextStep = stepJson[NEXT_STEP][RDFTerms.ID];
     }

     if (stepJson.containsKey(PREVIOUS_STEP)) {
       this.previousStep = stepJson[PREVIOUS_STEP][RDFTerms.ID];
     }
   }

   Map toJson() => {
     "title": this.title,
     "description": this.description
   };

   Map toEditionJson() {

     Map json = Map();
     json["id"] = this.id;
     json["title"] = this.title;
     json["description"] = this.description;

     // CU: All steps - {first step} has a previous Step property
     // CU: if a Step of the chain is moved to first place, It will send blank string to remove the property.
     json["previous_step"] = this.previousStep;

     return json;
   }
}