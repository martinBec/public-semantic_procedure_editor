class User {
  final String username;
  final String profileUrl = 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSUAqNQhi21yDW9avKKb7LZr6oNiU7KfDaykDKTBpc8YMfMj3waJyxodBc9n1y1hb_uD3w&usqp=CAU';
  final String token;

  const User({
    required this.username,
    required this.token
  });

  factory User.fromJson(Map<String, dynamic> json) {
    return User(
      username: json['username'],
        token: json['token']
    );
  }
}