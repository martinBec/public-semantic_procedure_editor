
class OntologyTerms {
  static const String PROCEDURE = "ars:Procedure";
  static const String STEP = "ars:Step";
}

class RDFTerms {
  static const String ID = "@id";
  static const String TYPE = "@type";
}
