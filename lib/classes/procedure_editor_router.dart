

import 'package:flutter/material.dart';
import 'package:semantic_procedure_editor/blocs/procedure_creation_editor_bloc.dart';
import 'package:semantic_procedure_editor/screens/home_widget.dart';
import '../blocs/procedure_modification_editor_bloc.dart';
import '../screens/login_widget.dart';
import '../screens/procedure_editor_widget.dart';

class ProcedureEditorRouter {

  void presentProcedureCreationEditor(BuildContext context) {
    MaterialPageRoute editor = this._editor(true, "Create Procedure", ProcedureCreationEditorBLoC());
    Navigator.push(context, editor);
  }

  void pushProcedureModificationEditor(String procedureLocalId, BuildContext context) {
    MaterialPageRoute editor = this._editor(true, "Modify Procedure", ProcedureModificationEditorBLoC(procedureLocalId));
    Navigator.push(context, editor);
  }

  void replaceFor(ProcedureEditorScreen screen, BuildContext context) {
    Navigator.pushReplacement(context,_pageFor(screen,false));
  }

  void present(ProcedureEditorScreen screen, BuildContext context) {
    Navigator.push(context, _pageFor(screen,true));
  }

  void pushTo(ProcedureEditorScreen screen, BuildContext context) {
    Navigator.push(context, _pageFor(screen,false));
  }

  MaterialPageRoute _pageFor(ProcedureEditorScreen screen,bool fullscreenDialog) {
    MaterialPageRoute page;
    switch (screen) {
      case ProcedureEditorScreen.login:
        page = _login(fullscreenDialog);
        break;
      case ProcedureEditorScreen.home:
        page = _home(fullscreenDialog);
        break;
    }
    return page;
  }

  MaterialPageRoute _login(bool fullscreenDialog) {
    print("login");
    return MaterialPageRoute(builder: (context) => LoginScreenWidget(title: "Login", buttonTitle: "Login"));
  }

  MaterialPageRoute _home(bool fullscreenDialog) {
    print("home");
    return MaterialPageRoute(builder: (context) => HomeWidget());
  }

  MaterialPageRoute _editor(bool fullscreenDialog, String title, ProcedureEditorBLoC editorBLoC) {
    print("editor");
    return MaterialPageRoute(
        builder: (context) => ProcedureEditorWidget(title:title, buttonTitle:"Save", editorBLoC: editorBLoC), fullscreenDialog: fullscreenDialog);
  }
}

enum ProcedureEditorScreen {
  login,
  home
}

ProcedureEditorRouter router = ProcedureEditorRouter();
