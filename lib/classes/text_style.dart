
import 'package:flutter/material.dart';

class TextStyleSheet {

  // Lists
  static TextStyle procedureListItemTitleStyle = const TextStyle( fontWeight: FontWeight.bold, fontSize: 18.0);
  static TextStyle procedureListItemTagStyle = const TextStyle(fontSize: 14.0);

  // Procedure edition sections
  static TextStyle sectionPrimaryTitleStyle = const TextStyle(fontSize: 22.0);
  static TextStyle sectionSecondaryTitleStyle = const TextStyle(fontSize: 18.0);
  static TextStyle stepOrderNumberStyle = const TextStyle(fontSize: 16.0, fontWeight: FontWeight.bold, color: Colors.white);
  static TextStyle placeholderStyle = const TextStyle(color: Colors.grey);

  // Alerts
  static TextStyle alertTitleStyle = const TextStyle(fontSize: 18.0, color: Colors.black54);
  static TextStyle alertDescriptionStyle = const TextStyle(fontSize: 16.0,color: Colors.black54);

}