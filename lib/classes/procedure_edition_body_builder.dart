import 'package:semantic_procedure_editor/classes/procedure_edition_controller.dart';
import '../model/procedure.dart';

class ProcedureEditionBodyBuilder {

  static const String PROCEDURE = "procedure";
  static const String STEPS_TO_CREATE = "steps_to_create";
  static const String STEPS_TO_MODIFY = "steps_to_modify";
  static const String STEPS_TO_DELETE = "steps_to_delete";

  static Map build(Procedure procedure) {

    Map bodyJson = Map();
    
    bodyJson[PROCEDURE] = procedure.toEditionJson();
    ProcedureEditionStepJSONBuilderInterface builder = procedure.steps.editionTracerToBuildEditionBody();
    Iterable<Map> stepsToCreateList = builder.buildStepsToCreate();
    
    if (stepsToCreateList.isNotEmpty) {
      List<Map> stepToCreateJsonList = stepsToCreateList.toList();
      bodyJson[STEPS_TO_CREATE] = stepToCreateJsonList;
    }

    Iterable<Map> stepsToModify = builder.buildStepsToModify(procedure.steps.toList());

    if (stepsToModify.isNotEmpty) {
      List<Map> stepToModifyJsonList = stepsToModify.toList();
      bodyJson[STEPS_TO_MODIFY] = stepToModifyJsonList;
    }

    Iterable<String> stepsToDelete = builder.buildStepsToDelete();

    if (stepsToDelete.isNotEmpty) {
      bodyJson[STEPS_TO_DELETE] = stepsToDelete.toList();
    }

    return bodyJson;
  }
}