import 'dart:collection';
import 'dart:core';
import 'dart:math';

import 'package:semantic_procedure_editor/classes/procedure_edition_controller.dart';
import 'package:semantic_procedure_editor/model/step.dart';

class ProxyStepList implements ListBase<Step> {

  Step? _firstStep;
  Step? _lastStepProcessed;
  HashMap<String,Step> _stepsMap = HashMap<String,Step>();
  List<Step> _steps = [];
  int _processedIndex = -1;
  ProcedureEditionController _editionController = ProcedureEditionController();

  ProxyStepList();

  ProxyStepList.initWithLocalListSteps(List<Step> steps) {

    if (_steps.isNotEmpty) {
      this._firstStep = steps.first;
      this._processedIndex = steps.length - 1;
    }
    this._steps = steps;
  }

  ProxyStepList.initWithRemoteSteps(Step? firstStep, HashMap<String,Step> stepsMap) {
    this._firstStep = firstStep;
    this._stepsMap = stepsMap;

    if (firstStep != null) {
      this._lastStepProcessed = firstStep;
      this._steps.add(firstStep);
      this._processedIndex = 0;
    }
  }

  _processStepFromStartToIndex(int index) {

    if (index <= _processedIndex) { return; }

    int start = this._processedIndex + 1;
    int stop = min(this._stepsMap.keys.length, index);

    if (start > stop) { return; }

    for (int i = start; i <= stop; i++) {

      String? nextStepLocalId = this._lastStepProcessed?.nextStep;

      if (nextStepLocalId == null) { return; }

      Step? step = this._stepsMap[nextStepLocalId];

      if (step != null) {
        this._steps.add(step);
        this._processedIndex += 1;
        this._lastStepProcessed = step;
      }
    }
  }

  ProcedureEditionStepJSONBuilderInterface editionTracerToBuildEditionBody() {
     return _editionController;
  }

  @override
  bool contains(Object? element) {
    bool containsStep = false;

    Step? step = element as Step?;

    if (step != null) {
      containsStep = this._stepsMap.containsKey(step.id);
    }

    return containsStep;
  }

  add(Step? step) {
    _processAllList();

    if (step == null) { return; }
    if (step.id.isEmpty) { return; }

    this._stepsMap[step.id] = step;

    if (this._steps.isNotEmpty) {
      this._steps.last.nextStep = step.id;
      step.previousStep = this._steps.last.id;
    }
    else {
      _firstStep = step;
    }

    this._steps.add(step);
    this._processedIndex = this._steps.length;
    _editionController.addStepToCreate(step);
  }

  Step operator [](int index) {
    return this.elementAt(index);
  }

  @override
  elementAt(int index) {
    _processStepFromStartToIndex(index);
    return _steps[index];
  }

  @override
  get first => this._firstStep!;

  @override
  get last => _processAllList().last;

  @override
  bool get isEmpty => this._steps.isEmpty || this._stepsMap.isEmpty;

  @override
  bool get isNotEmpty => this.isEmpty == false;

  @override
  int get length => max(this._stepsMap.length, this._steps.length);

  List<Step> _processAllList() {
    this._processStepFromStartToIndex(this._stepsMap.length - 1);
    return _steps;
  }

  @override
  void operator []=(int index, Step value) {
     this.insert(index, value);
  }

  @override
  void insert(int index, Step element) {
    _processAllList();

    if (element.id.isEmpty) { return; }
    if (index > this._stepsMap.length) { return; }

    this._stepsMap[element.id] = element;

    if (index == this._stepsMap.length) { // Append
      this.add(element);
      return;
    }

    if (index == 0) { // First Step Edition
      element.nextStep = this._firstStep?.id ?? "";
      this._firstStep = element;
    }
    else { // Normal Edition
      Step previousStep = this._steps[index - 1];
      element.nextStep = previousStep.nextStep;
      previousStep.nextStep = element.id;
      element.previousStep = previousStep.id;
    }

    this._steps.insert(index, element);
    this._processedIndex = this._steps.length;
    this._editionController.addStepToCreate(element);
  }

  void moveFrom(int index, int toIndex) {
     this._processAllList();

     if (index > this._steps.length) { return; }
     if (toIndex > this._steps.length) { return; }
     if (toIndex < 0) { return; }

     Step stepToMove = this._steps.removeAt(index);
     this._steps.insert(toIndex, stepToMove);

     for (int i = this._steps.length - 1; i > 0 ; i--) {

        Step stepToCorrect = this._steps[i];
        Step previousStep = this._steps[i - 1];

        if (stepToCorrect.previousStep != previousStep.id) {
          stepToCorrect.previousStep = previousStep.id;
          stepToCorrect.modified = true;
        }
     }

     if (this._firstStep?.id != this._steps.first.id) {
       this._firstStep = this._steps.first;
       this._firstStep?.previousStep = "";
       this._firstStep?.modified = true;
     }

  }

  @override
  Step removeAt(int index) {

    _processAllList();

    if (_steps.isEmpty) { throw Exception("Index out of bound exception"); }
    if (index >= _steps.length) { throw Exception("Index out of bound exception"); }

    Step stepToRemove;

    if (index == 0 && _steps.length > 1) {
       this._firstStep = _steps[1];
    }
    else if (index == 0) {
      this._firstStep = null;
    }

    bool moreOfOneStep = _steps.length > 1;
    bool thereIsNextSteps = index < _steps.length - 1;

    if (index > 0 &&  thereIsNextSteps && moreOfOneStep) {
      Step nextStep = _steps[index + 1];
      stepToRemove = _steps[index];
      nextStep.previousStep = stepToRemove.previousStep;
    }

    stepToRemove = _steps.removeAt(index);
    _stepsMap.remove(stepToRemove.id);
    _editionController.addStepToDelete(stepToRemove);
    return stepToRemove;
  }

  @override
  bool remove(Object? element) {
    _processAllList();

    if (element == null) { return false; }
    if (!(element is Step)) { return false; }

    int index = _steps.indexOf(element);

    if (index == -1 ) {
      return false;
    }

    this.removeAt(index);
    return true;
  }

  @override
  void clear() {
    // TODO: implement clear
  }

  @override
  List<Step> operator +(List<Step> other) {
    throw UnimplementedError();
  }

  @override
  set first(Step value) {
    throw UnimplementedError();
  }
  @override
  set last(Step value) {
    throw UnimplementedError();
  }

  @override
  set length(int newLength) {
    throw UnimplementedError();
  }

  @override
  void addAll(Iterable<Step> iterable) {
    throw Exception("Invalid Proxy list operation");
  }

  @override
  void insertAll(int index, Iterable<Step> iterable) {
    throw UnimplementedError();
  }

  @override
  void fillRange(int start, int end, [Step? fill]) {
    throw Exception("Invalid Proxy list operation");
  }

  @override
  Step removeLast() {
    throw UnimplementedError();
  }

  @override
  void removeRange(int start, int end) {
    throw Exception("Invalid Proxy list operation");
  }

  @override
  void removeWhere(bool Function(Step element) test) {
    throw Exception("Invalid Proxy list operation");
  }

  @override
  void replaceRange(int start, int end, Iterable<Step> newContents) {
    throw Exception("Invalid Proxy list operation");
  }

  @override
  void retainWhere(bool Function(Step element) test) {
    throw Exception("Invalid Proxy list operation");
  }

  @override
  void setAll(int index, Iterable<Step> iterable) {
    throw Exception("Invalid Proxy list operation");
  }

  @override
  void setRange(int start, int end, Iterable<Step> iterable, [int skipCount = 0]) {
    throw Exception("Invalid Proxy list operation");
  }

  @override
  void shuffle([Random? random]) {
    throw Exception("Invalid Proxy list operation");
  }

  @override
  void sort([int Function(Step a, Step b)? compare]) {
    throw Exception("Invalid Proxy list operation");
  }

  @override
  List<Step> sublist(int start, [int? end]) {
    return _processAllList().sublist(start,end);
  }

  @override
  Set<Step> toSet() {
    return _stepsMap.values.toSet();
  }

  @override
  List<Step> toList({bool growable = true}) {
    return _processAllList()
        .toList(growable: growable);
  }

  @override
  Iterable<T> map<T>(T Function(Step e) toElement) {
    return _processAllList()
        .map(toElement);
  }

  @override
  void forEach(void Function(Step element) action) {
    return _processAllList()
        .forEach(action);
  }

  @override
  Iterator<Step> get iterator => _processAllList().iterator;

  @override
  get single => _processAllList().single;

  @override
  Iterable<Step> skip(int count) {
    return _processAllList().skip(count);
  }

  @override
  Iterable<Step> take(int count) {
    return _processAllList().take(count);
  }

  @override
  bool any(bool Function(Step element) test) {
    return _processAllList().any(test);
  }

  @override
  List<R> cast<R>() {
    return _processAllList().cast<R>();
  }

  @override
  bool every(bool Function(Step element) test) {
    return _processAllList().every(test);
  }

  @override
  Iterable<T> expand<T>(Iterable<T> Function(Step element) toElements) {
    return _processAllList().expand(toElements);
  }

  @override
  Iterable<Step> followedBy(Iterable<Step> other) {
    return _processAllList().followedBy(other);
  }

  @override
  String join([String separator = ""]) {
    return _processAllList().join(separator);
  }

  @override
  lastWhere(bool Function(Step element) test, {Function()? orElse}) {
    return _processAllList().lastWhere(test);
  }


  @override
  reduce(Step Function(Step value, Step element) combine) {
    throw Exception("Invalid Proxy list operation");
  }

  @override
  singleWhere(bool Function(Step element) test, {Function()? orElse}) {
    return _processAllList().singleWhere(test);
  }

  @override
  Iterable<Step> skipWhile(bool Function(Step value) test) {
    return _processAllList().skipWhile(test);
  }

  @override
  Iterable<Step> takeWhile(bool Function(Step value) test) {
    return _processAllList().takeWhile(test);
  }

  @override
  Iterable<Step> where(bool Function(Step element) test) {
    return _processAllList().where(test);
  }

  @override
  Iterable<T> whereType<T>() {
    return _processAllList().whereType<T>();
  }

  @override
  firstWhere(bool Function(Step element) test, {Function()? orElse}) {
    return _processAllList().firstWhere(test);
  }

  @override
  T fold<T>(T initialValue, T Function(T previousValue, Step element) combine) {
    return _processAllList().fold(initialValue, combine);
  }

  @override
  Map<int, Step> asMap() {
    return _processAllList().asMap();
  }

  @override
  Iterable<Step> getRange(int start, int end) {
    return _processAllList().getRange(start, end);
  }

  @override
  int indexOf(Object? element, [int start = 0]) {
    if (element == null) return - 1;
    if (!(element is Step)) return - 1;
    return _processAllList().indexOf(element,start);
  }

  @override
  int indexWhere(bool Function(Step element) test, [int start = 0]) {
    return _processAllList().indexWhere(test, start);
  }

  @override
  int lastIndexOf(Object? element, [int? start]) {
    if (element == null) return - 1;
    if (!(element is Step)) return - 1;
    return _processAllList().lastIndexOf(element,start);
  }

  @override
  int lastIndexWhere(bool Function(Step element) test, [int? start]) {
    return _processAllList().lastIndexWhere(test,start);
  }

  @override
  Iterable<Step> get reversed => _processAllList().reversed;

}

