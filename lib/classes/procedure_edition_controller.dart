import 'package:semantic_procedure_editor/model/step.dart';

abstract class ProcedureEditionStepJSONBuilderInterface
{
  Iterable<Map> buildStepsToCreate ();
  Iterable<Map> buildStepsToModify (List<Step> procedureSteps);
  Iterable<String> buildStepsToDelete ();
}

abstract class ProcedureEditionTracerInterface
{
  void addStepToCreate(Step step);
  void addStepToDelete(Step step);
  void clear ();
}

class ProcedureEditionController implements ProcedureEditionTracerInterface, ProcedureEditionStepJSONBuilderInterface  {

  Map<String, Step> _stepToCreate = Map<String, Step>();
  Map<String, Step> _stepToDelete = Map<String, Step>();

  ProcedureEditionController();

  void addStepToCreate(Step step) {
    if (step.id.isNotEmpty) {
      _stepToCreate[step.id] = step;
    }
  }

  void addStepToDelete(Step step) {

    if (_stepToCreate.containsKey(step.id)) {
      _stepToCreate.remove(step.id);
    }

    if (step.id.isNotEmpty) {
      _stepToDelete[step.id] = step;
    }
  }

  void clear () {
    _stepToCreate.clear();
    _stepToDelete.clear();
  }

  Iterable<Map> buildStepsToCreate () {
    return _stepToCreate.values.map((step) => step.toEditionJson());
  }

  Iterable<Map> buildStepsToModify (List<Step> procedureSteps) {
    return procedureSteps
        .where((step) => !_stepToCreate.containsKey(step.id))
        .where((step) => !_stepToDelete.containsKey(step.id))
        .where((step) => step.modified)
                         .map((step) => step.toEditionJson());
  }

  Iterable<String> buildStepsToDelete () {
    return _stepToDelete.keys;
  }
}