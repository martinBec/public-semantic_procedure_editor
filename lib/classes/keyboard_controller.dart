import 'package:keyboard_utils/keyboard_listener.dart';
import 'package:keyboard_utils/keyboard_utils.dart';

class KeyboardController {

  final _keyboardUtils = KeyboardUtils();

  bool keyboardVisible = false;
  int _keyboardObserverId = -1;
  Function(bool)? onChangeVisible;

  KeyboardController() {

    _keyboardObserverId = _keyboardUtils.add(listener: KeyboardListener(
        willHideKeyboard: () {
          keyboardVisible = false;
          _updateVisibleState();
        },
        willShowKeyboard: (double keyboardHeight) {
          keyboardVisible = true;
          _updateVisibleState();
    }));
  }

  void _updateVisibleState() {
    if (this.onChangeVisible != null) {
      this.onChangeVisible!(keyboardVisible);
    }
  }
  void dispose() {
    _keyboardUtils.unsubscribeListener(subscribingId: _keyboardObserverId);
    onChangeVisible = null;
  }
}