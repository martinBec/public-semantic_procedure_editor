import 'dart:async';
import 'package:flutter/material.dart';
import 'package:semantic_procedure_editor/classes/text_style.dart';
import 'package:semantic_procedure_editor/views/procedure_editor_textarea.dart';
import 'package:semantic_procedure_editor/views/procedure_editor_textfield.dart';
import 'package:rxdart/rxdart.dart';

class ProcedureBodyViewWidget extends StatefulWidget {

  ProcedureBodyViewWidget({
    Key? key,
    required this.title,
    required this.description,
    required this.onTitleUpdated,
    required this.onDescriptionUpdated
  }) : super(key: key);

  final String title;
  final String description;
  final void Function(String)? onTitleUpdated;
  final void Function(String)? onDescriptionUpdated;

  @override
  _ProcedureBodyViewState createState() {
    var state = _ProcedureBodyViewState();
    state.onTitleUpdated = this.onTitleUpdated;
    state.onDescriptionUpdated = this.onDescriptionUpdated;
    return state;
  }

}

class _ProcedureBodyViewState extends State<ProcedureBodyViewWidget> {

  final Padding _spaceBetweenTitleAndTxt = Padding(padding: EdgeInsets.only(top: 5));
  final Padding _formElementSpace = Padding(padding: EdgeInsets.only(top: 20));

  final TextEditingController _titleController = TextEditingController();
  final TextEditingController _descriptionController = TextEditingController();
  final StreamController<String> _titleStreamController = StreamController();
  final StreamController<String> _descriptionStreamController = StreamController();

  late void Function(String)? onTitleUpdated;
  late void Function(String)? onDescriptionUpdated;

  void initState() {
    super.initState();
    this._subscribeEvents();
  }


  @override
  void dispose() {
    super.dispose();
    this._closeEventSubscriptions();
  }

  void _subscribeEvents() {

    _titleStreamController
        .stream
        .skip(1)
        .debounce((_) => TimerStream(true, Duration(milliseconds: 600)))
        .listen((event) {
      _updateTitle(event);
    });

    _descriptionStreamController
        .stream
        .skip(1)
        .debounce((_) => TimerStream(true, Duration(milliseconds: 600)))
        .listen((event) {
      _updateDescription(event);
    });

    _titleController.addListener(() {
      _titleStreamController.add(_titleController.text);
    });

    _descriptionController.addListener(() {
      _descriptionStreamController.add(_descriptionController.text);
    });
  }

  void _updateTitle(String title) {
    print(title);
    if (this.onTitleUpdated != null) {
      this.onTitleUpdated!(title);
    }
  }

  void _updateDescription(String description) {
    print(description);
    if (this.onDescriptionUpdated != null) {
      this.onDescriptionUpdated!(description);
    }
  }

  void _closeEventSubscriptions() {
    _titleStreamController.close();
    _descriptionStreamController.close();
    _titleController.dispose();
    _descriptionController.dispose();
    onTitleUpdated = null;
    onDescriptionUpdated = null;
  }

  @override
  Widget build(BuildContext context) {

    this._titleController.text = widget.title;
    this._descriptionController.text = widget.description;

    return Card(
      elevation: 0,
      margin: EdgeInsets.zero,
      child: Padding(
          padding: EdgeInsets.all(25),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text("Title", style: TextStyleSheet.sectionSecondaryTitleStyle),
              _spaceBetweenTitleAndTxt,
              ProcedureEditorTextFieldWidget(
                  placeholder: "Write your title here...",
                  controller: _titleController),
              _formElementSpace,
              Text("Description", style: TextStyleSheet.sectionSecondaryTitleStyle),
              _spaceBetweenTitleAndTxt,
              ProcedureEditorTextAreaWidget(
                  placeholder: "Write your description here...",
                  controller: _descriptionController)
            ],
          )),
    );
  }
}