import 'package:flutter/material.dart';
import 'package:semantic_procedure_editor/classes/text_style.dart';

class LoginTextFieldWidget extends StatelessWidget {

  const LoginTextFieldWidget({
    Key? key,
    required this.placeholder,
    required this.controller,
    required this.textfieldType
  }) : super(key: key);

  final String placeholder;
  final TextEditingController controller;
  final LoginTextfieldType textfieldType;

  @override
  Widget build(BuildContext context) {
    return TextField(
      decoration: InputDecoration(
          hintStyle: TextStyleSheet.placeholderStyle,
          hintText: placeholder
      ),
      obscureText: textfieldType == LoginTextfieldType.password,
      enableSuggestions: false,
      autocorrect: false,
      controller: controller,
      maxLength: 40,
      maxLines: 1,
    );
  }
}

enum LoginTextfieldType {
   username,
   password
}