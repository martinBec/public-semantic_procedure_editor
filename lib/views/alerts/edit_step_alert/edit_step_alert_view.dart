

import 'package:flutter/material.dart';
import 'package:semantic_procedure_editor/classes/text_style.dart';

abstract class EditStepAlertViewActions {

  void move(int index);
  void delete(int index);
  void cancel(int index);

}

class EditStepAlertView {

  static showAlertOn(BuildContext context, EditStepAlertViewActions actions, int stepIndex) {
    showModalBottomSheet<void>(
        context: context,
        builder: builder(context, stepIndex, actions)
    );
  }

  static WidgetBuilder builder(BuildContext context,int stepIndex, EditStepAlertViewActions actions) {

    return (BuildContext context) {
      return Container(
        color: Colors.white,
        padding: EdgeInsets.all(20.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text('Actions', style: TextStyleSheet.alertTitleStyle),
            SizedBox(height: 20.0),
            Text('Select any of the actions below to proceed.', style: TextStyleSheet.alertDescriptionStyle),
            SizedBox(height: 20.0),
            _swapActionRow(context, stepIndex, actions),
            _deleteActionRow(context, stepIndex, actions),
            _cancelActionRow(context, stepIndex, actions),
          ],
        ),
      );
    };
  }

  static Widget _swapActionRow(BuildContext context, int index, EditStepAlertViewActions actions) {
    return _actionRow(context, Icons.swap_vert, "Move step", index, actions.move);
  }

  static Widget _deleteActionRow(BuildContext context, int index, EditStepAlertViewActions actions) {
    return _actionRow(context, Icons.delete, "Delete step", index, actions.delete);
  }

  static Widget _cancelActionRow(BuildContext context, int index, EditStepAlertViewActions actions) {
    return _actionRow(context, Icons.close, "Cancel", index, actions.cancel);
  }

  static Widget _actionRow(BuildContext context, IconData iconData, String title, int index, Function(int index) action) {
    return InkWell(
      child: Container(child:
      Row(children: [
        Icon(iconData),
        SizedBox(width: 10),
        Text(title)
      ]),
        padding: EdgeInsets.only(top: 10.0,bottom: 10),
      )
      ,onTap: () {
      Navigator.pop(context);
      action(index);
    },
    );
  }
}