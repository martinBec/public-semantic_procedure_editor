import 'dart:ffi';

import 'package:semantic_procedure_editor/model/step.dart';

 class StepSelectorAlertItem {
  final Step? step;
  final int index;

  const StepSelectorAlertItem({
    required this.step,
    required this.index
  });

  @override
  String toString() {

   String title = "No value selected";

   if (step?.title != null) {
    int indexToShow = index + 1;
    title = "$indexToShow - " + step!.title;
   }

   return title;
  }

  @override
  operator ==(o) => o is StepSelectorAlertItem && o.index == index;

  @override
  int get hashCode => index.hashCode;

}