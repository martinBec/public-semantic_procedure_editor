import 'package:flutter/widgets.dart';
import 'package:select_dialog/select_dialog.dart';
import 'package:semantic_procedure_editor/classes/proxy_step_list.dart';
import 'package:semantic_procedure_editor/views/alerts/step_selector_alert/step_selector_alert_item.dart';

class StepSelectorAlert {

  static showAlert(ProxyStepList steps, Function (StepSelectorAlertItem selectedItem) onItemSelected, BuildContext context) {

    StepSelectorAlertItem defaultItem = StepSelectorAlertItem(step: null, index: -1);
    int index = 0;

    List<StepSelectorAlertItem> stepSelectorItemList = steps.map((step) => StepSelectorAlertItem(step:step, index: index++)).toList();
    stepSelectorItemList.insert(0, defaultItem);

    SelectDialog.showModal<StepSelectorAlertItem>(
      context,
      label: "Move Step to",
      selectedValue: defaultItem,
      items: stepSelectorItemList,
      onChange: (StepSelectorAlertItem selected) {
        onItemSelected(selected);
      },
    );
  }
}