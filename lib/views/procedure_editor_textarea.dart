import 'package:flutter/material.dart';
import 'package:semantic_procedure_editor/classes/text_style.dart';

class ProcedureEditorTextAreaWidget extends StatelessWidget {

  const ProcedureEditorTextAreaWidget({
    Key? key,
    required this.placeholder,
    required this.controller
  }) : super(key: key);

  final String placeholder;
  final TextEditingController controller;

  @override
  Widget build(BuildContext context) {
    return TextField(
      decoration: InputDecoration(
          hintStyle: TextStyleSheet.placeholderStyle,
          hintText: placeholder,
      ),
      controller: controller,
      maxLength: 280,
      minLines: 1,
      maxLines: 4,
    );
  }
}