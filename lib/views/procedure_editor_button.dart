
import 'package:flutter/material.dart';

class ProcedureEditorButton extends StatelessWidget {

  const ProcedureEditorButton ({
    Key? key,
    required this.buttonTitle,
    required this.onPressed
  }): super(key: key);

  final String buttonTitle;
  final void Function() onPressed;

  @override
  Widget build(BuildContext context) {
    return ConstrainedBox(
        constraints: BoxConstraints(minWidth: 250.0),
        child: ElevatedButton(
            child: Text(buttonTitle), onPressed: onPressed)
    );
  }

}