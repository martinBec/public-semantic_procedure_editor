import 'package:flutter/material.dart';
import 'package:semantic_procedure_editor/classes/text_style.dart';

class MyProcedureCellWidget extends StatelessWidget {

  const MyProcedureCellWidget({
    Key? key,
    required this.title,
    required this.subtitle,
    required this.url,
  }) : super(key: key);

  final String title;
  final String subtitle;
  final String url;

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Card(
        child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Expanded(
                flex: 2,
                child: Container(
                  margin: EdgeInsets.all(15.0),
                  child: _ProcedureImageWidget(url:url)),
              ),
              Expanded(
                flex: 4,
                child: _ProcedureDescription(title: title, subtitle: subtitle),
              )
            ]),
        elevation: 5.0,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(5),
        ),
        margin: EdgeInsets.fromLTRB(15.0, 0.0, 15.0, 15.0),
      ),
    );
  }
}

class _ProcedureImageWidget extends StatelessWidget {

  const _ProcedureImageWidget({
    Key? key,
    required this.url,
  }) : super(key: key);

  final String url;

  @override
  Widget build(BuildContext context) {
    return ClipRRect(
        child: Image.network(url,
            height: 90, fit: BoxFit.cover),
        borderRadius: BorderRadius.circular(5.0)
    );
  }
}

class _ProcedureDescription extends StatelessWidget {

  const _ProcedureDescription({
    Key? key,
    required this.title,
    required this.subtitle,
  }) : super(key: key);

  final String title;
  final String subtitle;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(5.0, 0.0, 0.0, 0.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            title,
            style: TextStyleSheet.procedureListItemTitleStyle,
          ),
          const Padding(padding: EdgeInsets.symmetric(vertical: 1.0)),
          Text(
            subtitle,
            style: TextStyleSheet.procedureListItemTagStyle,
          )
        ],
      ),
    );
  }
}
