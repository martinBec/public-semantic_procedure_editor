import 'dart:async';
import 'package:flutter/material.dart';
import 'package:semantic_procedure_editor/classes/text_style.dart';
import 'package:semantic_procedure_editor/views/alerts/edit_step_alert/edit_step_alert_view.dart';
import 'package:semantic_procedure_editor/views/procedure_editor_textarea.dart';
import 'package:semantic_procedure_editor/views/procedure_editor_textfield.dart';
import 'package:rxdart/rxdart.dart';

class EditorItemViewWidget extends StatefulWidget {

  EditorItemViewWidget({
    Key? key,
    required this.title,
    required this.description,
    required this.order,
    required this.onTitleUpdated,
    required this.onDescriptionUpdated,
    required this.editStepActions,
  }) : super(key: key);

  final String title;
  final String description;
  final int order;
  final void Function(String)? onTitleUpdated;
  final void Function(String)? onDescriptionUpdated;
  final EditStepAlertViewActions editStepActions;

  @override
  _EditorItemViewState createState() {
    var state = _EditorItemViewState();
    state.onTitleUpdated = this.onTitleUpdated;
    state.onDescriptionUpdated = this.onDescriptionUpdated;
    return state;
  }

}

class _EditorItemViewState extends State<EditorItemViewWidget> {

  final Padding _spaceBetweenTitleAndTxt = Padding(padding: EdgeInsets.only(top: 5));
  final Padding _formElementSpace = Padding(padding: EdgeInsets.only(top: 20));
  final TextEditingController _titleController = TextEditingController();
  final TextEditingController _descriptionController = TextEditingController();
  final StreamController<String> _titleStreamController = StreamController();
  final StreamController<String> _descriptionStreamController = StreamController();

  late void Function(String)? onTitleUpdated;
  late void Function(String)? onDescriptionUpdated;

  void initState() {
    super.initState();
    this._subscribeEvents();
  }


  @override
  void dispose() {
    super.dispose();
    this._closeEventSubscriptions();
  }

  void _subscribeEvents() {

    _titleStreamController
        .stream
        .skip(1)
        .debounce((_) => TimerStream(true, Duration(milliseconds: 600)))
        .listen((event) {
          _updateTitle(event);
        });

    _descriptionStreamController
        .stream
        .skip(1)
        .debounce((_) => TimerStream(true, Duration(milliseconds: 600)))
        .listen((event) {
          _updateDescription(event);
        });

    _titleController.addListener(() {
      _titleStreamController.add(_titleController.text);
    });

    _descriptionController.addListener(() {
      _descriptionStreamController.add(_descriptionController.text);
    });
  }

  void _updateTitle(String title) {
    print(title);
    if (this.onTitleUpdated != null) {
      this.onTitleUpdated!(title);
    }
  }

  void _updateDescription(String description) {
    print(description);
    if (this.onDescriptionUpdated != null) {
      this.onDescriptionUpdated!(description);
    }
  }

  void _closeEventSubscriptions() {
    _titleStreamController.close();
    _descriptionStreamController.close();
    _titleController.dispose();
    _descriptionController.dispose();
    onTitleUpdated = null;
    onDescriptionUpdated = null;
  }

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 0,
      margin: EdgeInsets.zero,
      child: Padding(
          padding: EdgeInsets.only(top: 25,left: 10,right: 25.0,bottom: 25.0),
          child: itemRow(this.widget.order - 1)
      ),
    );
  }


  Widget itemRow(int index) {
    return Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          this.itemOrderView(),
          this.itemViewTextFields(index)
    ]);
  }

  Widget itemOrderView() {

    return Flexible(
        child: Column(
      children: <Widget>[
        SizedBox(height: 5),
        Container(
          padding: EdgeInsets.all(0),
          decoration: BoxDecoration(color: Colors.black, shape: BoxShape.circle),
          child: ClipOval(
            child: SizedBox.fromSize(
              size: Size.fromRadius(14), // Image radius
              child: Center(
                child: Text(this.widget.order.toString(), textAlign: TextAlign.center, style: TextStyleSheet.stepOrderNumberStyle)
              ),
            ),
          ),
        )
      ],
    )
    );
  }

  Widget itemViewTextFields(int index) {

    this._titleController.text = widget.title;
    this._descriptionController.text = widget.description;

    return Flexible(
      flex: 9,
        child:Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Text("Title", style: TextStyleSheet.sectionSecondaryTitleStyle),
            IconButton(
                onPressed: (){
                     EditStepAlertView.showAlertOn(context, this.widget.editStepActions, index);
                  },
                icon: Icon(Icons.more_horiz, color: Colors.black),
              constraints: BoxConstraints(maxWidth: 30),
            )
          ],
        ),
        _spaceBetweenTitleAndTxt,
        ProcedureEditorTextFieldWidget(
            placeholder: "Write your title here...",
            controller: _titleController),
        _formElementSpace,
        Text("Description", style: TextStyleSheet.sectionSecondaryTitleStyle),
        _spaceBetweenTitleAndTxt,
        ProcedureEditorTextAreaWidget(
            placeholder: "Write your description here...",
            controller: _descriptionController)
      ],
    )
    );
  }

}