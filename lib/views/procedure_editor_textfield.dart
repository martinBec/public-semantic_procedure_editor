import 'package:flutter/material.dart';
import 'package:semantic_procedure_editor/classes/text_style.dart';

class ProcedureEditorTextFieldWidget extends StatelessWidget {

  const ProcedureEditorTextFieldWidget({
    Key? key,
    required this.placeholder,
    required this.controller
  }) : super(key: key);

  final String placeholder;
  final TextEditingController controller;

  @override
  Widget build(BuildContext context) {
    return TextField(
      decoration: InputDecoration(
          hintStyle: TextStyleSheet.placeholderStyle,
          hintText: placeholder
      ),
      controller: controller,
      maxLength: 40,
      maxLines: 1,
    );
  }
}
