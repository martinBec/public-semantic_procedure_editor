
import 'package:flutter/material.dart';
import 'package:semantic_procedure_editor/classes/text_style.dart';

class ProfileHeaderWidget extends StatelessWidget {

  ProfileHeaderWidget({
    Key? key,
    required this.username,
    required this.profileUrl
  }) : super(key: key);

  final String username;
  final String profileUrl;
  @override
  Widget build(BuildContext context) {
    
    return Container(child: 
    Column( children: [
    Padding(padding: EdgeInsets.only(top: 30, bottom: 15), child:
      Container(
        decoration: BoxDecoration(
          image: DecorationImage( image: NetworkImage(this.profileUrl), fit: BoxFit.scaleDown),
          color: Colors.white,
          shape: BoxShape.circle,
          ),height: 125,
      )),
      Text(username,style: TextStyleSheet.sectionSecondaryTitleStyle)
    ]));
  }
}
