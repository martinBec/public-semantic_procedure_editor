import 'dart:async';
import 'package:flutter/material.dart';
import 'package:semantic_procedure_editor/blocs/procedure_list_bloc.dart';
import '../classes/procedure_editor_router.dart';
import '../model/procedure.dart';
import '../views/cells/my_procedure_cell_widget.dart';

class MyProcedureListWidget extends StatefulWidget {
  @override
  _MyProcedureListState createState() => _MyProcedureListState();
}

class _MyProcedureListState extends State<MyProcedureListWidget> {

  List<Procedure> procedures = [];
  late StreamSubscription<List<Procedure>>? _proceduresUpdatesSubscription;

  @override
  void initState() {
    super.initState();

    this._proceduresUpdatesSubscription = procedureListBLoC.onProcedureListUpdates.listen((procedures) {
      setState(() {
        this.procedures = procedures;
      });
    });

    procedureListBLoC.onStart();
  }

  @override
  void dispose() {
    this._proceduresUpdatesSubscription?.cancel();
    this._proceduresUpdatesSubscription = null;
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("My Procedures"),
      ),
      body: Container(
          child: ListView.builder(
        itemCount: procedures.length,
        itemBuilder: (BuildContext context, int position) {
          return getRow(procedures[position]);
        },
        padding: EdgeInsets.only(top: 20),
      )),
    );
  }


  Widget getRow(Procedure procedure) {
    return GestureDetector(
      child: MyProcedureCellWidget(
          title: procedure.title,
          subtitle: "Smartphone",
          url: 'https://picsum.photos/250?image=9'
      ),
      onTap: () {
        this.fetchProcedure(procedure.id);
      },
    );
  }

  MockProcedure buildMock (int i) {
    return MockProcedure(title: "Example $i", subtitle: " subtitle $i", url: 'https://picsum.photos/250?image=9');
  }

  fetchProcedure(String localId) {
    router.pushProcedureModificationEditor(localId, context);
  }
}

class MockProcedure {
  const MockProcedure({ required this.title,required this.subtitle,required this.url});

  final String title;
  final String subtitle;
  final url;
}