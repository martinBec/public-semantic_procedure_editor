import 'package:flutter/material.dart';
import 'package:semantic_procedure_editor/blocs/user_session_bloc.dart';
import 'package:semantic_procedure_editor/model/credentials.dart';
import '../classes/procedure_editor_router.dart';

class InitialScreenWidget extends StatefulWidget {
  const InitialScreenWidget(
      {Key? key}) : super(key: key);

  @override
  _InitialScreenWidgetState createState() => _InitialScreenWidgetState();
}

class _InitialScreenWidgetState extends State<InitialScreenWidget> {

  @override
  void initState() {
    super.initState();
    this.loadSession();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: AppBar(
        title: Text(""),
      ),
      body: Container()
    );
  }

  void loadSession() {

    userSession.retrieveCredentials()
        .then((credentials) {
          if (credentials != null) {
            print("Credentials found, login will be performed.");
            this.performLoginRequest(credentials);
          }
          else {
            print("Credentials not found, login screen will be loaded.");
            router.replaceFor(ProcedureEditorScreen.login, context);
          }
        });
  }
  void performLoginRequest(Credentials credentials) {

    userSession.login(credentials)
        .then((loggedUser) {
          print("User logged, home will be loaded.");
          router.replaceFor(ProcedureEditorScreen.home, context);
        })
        .catchError((error) {
          print("Login request error, login screen will be loaded.");
          router.replaceFor(ProcedureEditorScreen.login, context);
        });
  }
}