import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_hud/flutter_hud.dart';
import 'package:semantic_procedure_editor/Classes/keyboard_controller.dart';
import 'package:semantic_procedure_editor/blocs/procedure_creation_editor_bloc.dart';
import 'package:semantic_procedure_editor/classes/proxy_step_list.dart';
import 'package:semantic_procedure_editor/classes/text_style.dart';
import 'package:semantic_procedure_editor/model/procedure.dart';
import 'package:semantic_procedure_editor/model/step.dart' as Model;
import 'package:semantic_procedure_editor/views/alerts/step_selector_alert/step_selector_alert.dart';
import 'package:semantic_procedure_editor/views/editor_item_view.dart';
import 'package:semantic_procedure_editor/views/procedure_editor_button.dart';
import 'package:semantic_procedure_editor/views/procedure_body_view.dart';
import '../views/alerts/edit_step_alert/edit_step_alert_view.dart';


class ProcedureEditorWidget extends StatefulWidget {
  ProcedureEditorWidget(
      {Key? key, required this.title, required this.buttonTitle, required this.editorBLoC})
      : super(key: key);

  final String title;
  final String buttonTitle;
  final ProcedureEditorBLoC editorBLoC;

  @override
  _ProcedureEditorState createState() => _ProcedureEditorState();
}

class _ProcedureEditorState extends State<ProcedureEditorWidget> implements EditStepAlertViewActions {

  final EdgeInsets formHorizontalSpace = EdgeInsets.only(left: 20.0, right: 20.0);
  bool _keyboardVisible = false;
  KeyboardController _keyboardController = KeyboardController();

  late StreamSubscription<Procedure?> _proceduresUpdatesSubscription;
  late StreamSubscription<bool> _loadingSubscription;

  @override
  void initState() {
    super.initState();

    widget.editorBLoC.start();

    _keyboardController.onChangeVisible =  (visible) {
      setState(() {
        _keyboardVisible = visible;
      });
    };

    _proceduresUpdatesSubscription = widget.editorBLoC.onCurrentProcedureUpdates.listen((procedure) {
      setState(() { });
    });

    subscribeToLoadingEvents();
  }

  @override
  void dispose() {
    _keyboardController.dispose();
    _proceduresUpdatesSubscription.cancel();
    _loadingSubscription.cancel();
    widget.editorBLoC.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {

    int itemCount = 2;
    ProxyStepList steps = widget.editorBLoC.currentProcedure?.steps ?? ProxyStepList();

    if(steps.length > 0) {
       itemCount += steps.length + 1;
    }

    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Expanded(
                child: ListView.builder(
                    itemCount: itemCount,
                    itemBuilder: (BuildContext context, int position) {

                        if(position == 0) { return buildHeader("Procedure", 0.0); }
                        if(position == 1) { return buildEditorItemWidget(); }
                        if(position == 2) { return buildHeader("Steps", 20.0);}

                        int stepPosition = position - 3;
                        var step = steps[stepPosition];
                        return buildStepItemWidget(step, stepPosition);
                    })
            ),
            Align(
              alignment: Alignment.bottomCenter,
              child: AddStepsFooterWidget(
                  buttonTitle: widget.buttonTitle,
                  keyboardVisible: _keyboardVisible,
                  onAddStepAction: () {
                    setState(() {
                      Model.Step step = Model.Step.draft();
                      steps.add(step);
                    });
                  },
                  onSaveAction: saveProcedure),
            )
          ],
        ),
      ),
    );
  }

  saveProcedure() {
    var hud = buildHud("Saving...");
    hud.show();
    widget.editorBLoC.performSaveRequest(onProcedureUploadFinished: (){
      hud.dismiss();
      Navigator.pop(context);
    });
  }

  ProcedureBodyViewWidget buildEditorItemWidget() {
    Procedure? procedure = this.widget.editorBLoC.currentProcedure;
    String title = procedure?.title ?? "";
    String description = procedure?.description ?? "";

    return ProcedureBodyViewWidget(
        title: title,
        description: description,
        onTitleUpdated: (title) {
          this.widget.editorBLoC.currentProcedure?.title = title;
        },
        onDescriptionUpdated: (description){
          this.widget.editorBLoC.currentProcedure?.description = description;
        });
  }

  StepItemEditorWidget buildStepItemWidget(Model.Step step, int position){
    return StepItemEditorWidget(title: step.title, description: step.description, order: position + 1,
        onTitleUpdated: (title) {
          this.widget.editorBLoC.currentProcedure?.steps[position].title = title;
          this.widget.editorBLoC.currentProcedure?.steps[position].modified = true;
        },
        onDescriptionUpdated: (description) {
          this.widget.editorBLoC.currentProcedure?.steps[position].description = description;
          this.widget.editorBLoC.currentProcedure?.steps[position].modified = true;
        },
        editStepActions: this
    );
  }

  Widget buildHeader(String title, double topMargin) {
    return Card(
        elevation: 0,
        margin: EdgeInsets.only(top: topMargin),
        child:Padding(
            padding: EdgeInsets.only(top: 20,left: 10,bottom: 10),
            child: Text(title, style: TextStyleSheet.sectionPrimaryTitleStyle)
        ));
  }

  PopupHUD buildHud(String title) {
    return PopupHUD(
      context,
      hud: HUD(
        label: title,
        detailLabel: '',
      ),
    );
  }

  void subscribeToLoadingEvents() {

    late PopupHUD hud;

    _loadingSubscription = this.widget.editorBLoC.onLoading.listen((loading) {

       if (loading) {
         hud = this.buildHud("Loading...");
         hud.show();
       }
       else {
         hud.dismiss();
       }
    });
  }

  @override
  void cancel(int index) {
    // Do Nothing
  }

  @override
  void delete(int index) {
    setState(() {
      widget.editorBLoC.currentProcedure?.steps.removeAt(index);
    });
  }

  @override
  void move(int index) {

    ProxyStepList? steps = widget.editorBLoC.currentProcedure?.steps;
    if (steps == null) { return; }

    StepSelectorAlert.showAlert(steps, (selectedItem) {

      setState(() {
        steps.moveFrom(index, selectedItem.index);
      });

    }, context
    );
  }
}

class StepItemEditorWidget extends StatefulWidget {

  final String title;
  final String description;
  final int order;
  final EditStepAlertViewActions editStepActions;

  final void Function(String)? onTitleUpdated;
  final void Function(String)? onDescriptionUpdated;

  StepItemEditorWidget(
      {Key? key, required this.title, required this.description, required this.order, required this.onTitleUpdated, required this.onDescriptionUpdated, required this.editStepActions})
      : super(key: key);

  @override
  _StepItemEditorState createState() {
    var state = _StepItemEditorState();
    state.onTitleUpdated = onTitleUpdated;
    state.onDescriptionUpdated = onDescriptionUpdated;
    return state;
  }

}

class _StepItemEditorState extends State<StepItemEditorWidget> {

  late void Function(String)? onTitleUpdated;
  late void Function(String)? onDescriptionUpdated;

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    onTitleUpdated = null;
    onDescriptionUpdated = null;
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Column(children: [
      Container(
          child: EditorItemViewWidget(
              title: widget.title,
              description: widget.description,
              order: widget.order,
              onTitleUpdated: (title) {
                this._updateTitle(title);
              },
              onDescriptionUpdated: (description) {
                this._updateDescription(description);
              },
              editStepActions: this.widget.editStepActions
          ),
          padding: EdgeInsets.only(top: 0, left: 0.0, right: 0.0)),
          Divider(height: 0.0, color: Colors.grey)
    ]);
  }

  void _updateTitle(String title) {
    if (this.onTitleUpdated != null) {
      this.onTitleUpdated!(title);
    }
  }

  void _updateDescription(String description) {
    if (this.onDescriptionUpdated != null) {
      this.onDescriptionUpdated!(description);
    }
  }

}

class AddStepsFooterWidget extends StatelessWidget {

  const AddStepsFooterWidget({ required this.buttonTitle, required this.keyboardVisible, required this.onAddStepAction, required this.onSaveAction });

  final bool keyboardVisible;
  final String buttonTitle;
  final void Function() onAddStepAction;
  final void Function() onSaveAction;

  @override
  Widget build(BuildContext context) {
    return Container(
        child: Column(children: buildChildList()),
        decoration: BoxDecoration(color: Colors.white,boxShadow: [
          BoxShadow(
            color: Colors.grey.withOpacity(0.5),
            spreadRadius: 5,
            blurRadius: 7.0, // changes position of shadow
          )
        ])
    );
  }

  List<Widget> buildChildList() {
    List<Widget> widgetList = [];
    widgetList.add(AddStepsWidget(onPressed: onAddStepAction));

    if (!this.keyboardVisible) {
      widgetList.add(ProcedureEditorButton(buttonTitle: buttonTitle, onPressed:(){
        this.onSaveAction();
      }));
      widgetList.add(const Padding(padding: EdgeInsets.only(bottom: 10.0)));
    }

    return widgetList;
  }
}

class AddStepsWidget extends StatelessWidget {
  const AddStepsWidget({required this.onPressed});
  final void Function() onPressed;

  @override
  Widget build(BuildContext context) {
    return Container(
        child: Row(children: [
          Expanded(child: Text("Add Steps")),
          AddImageButton(onPressed: onPressed)
        ])
        ,margin: EdgeInsets.only(left: 20.0));
  }
}

class AddImageButton extends StatelessWidget {
  const AddImageButton({required this.onPressed});

  final void Function() onPressed;

  @override
  Widget build(BuildContext context) {
    return TextButton(onPressed: onPressed, child: Icon(Icons.add_circle));
  }
}