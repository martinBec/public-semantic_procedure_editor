import 'package:flutter/material.dart';
import 'package:semantic_procedure_editor/blocs/user_session_bloc.dart';
import 'package:semantic_procedure_editor/model/credentials.dart';
import 'package:semantic_procedure_editor/views/login_textfield.dart';
import 'package:semantic_procedure_editor/views/procedure_editor_button.dart';
import '../classes/procedure_editor_router.dart';


class LoginScreenWidget extends StatefulWidget {
  const LoginScreenWidget(
      {Key? key, required this.title, required this.buttonTitle})
      : super(key: key);

  final String title;
  final String buttonTitle;

  @override
  _LoginScreenWidgetState createState() => _LoginScreenWidgetState();
}

class _LoginScreenWidgetState extends State<LoginScreenWidget> {
  final Padding topSpace = Padding(padding: EdgeInsets.only(top: 0));
  final EdgeInsets formHorizontalSpace =
  EdgeInsets.only(top:20.0, left: 20.0, right: 20.0, bottom: 10.0);

  final TextEditingController _usernameController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Container(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
             LoginTextFieldWidget(placeholder: "Username", controller: _usernameController, textfieldType: LoginTextfieldType.username),
              LoginTextFieldWidget(placeholder: "Password", controller: _passwordController, textfieldType: LoginTextfieldType.password),
              Expanded(
                  child: Align(
                    alignment: Alignment.bottomCenter,
                    child: ProcedureEditorButton(
                      buttonTitle: widget.buttonTitle,
                      onPressed: () {
                        this.performLoginRequest();
                      },
                    ),
                  ))
            ],
          ),
          padding: formHorizontalSpace
      ),
    );
  }

  void performLoginRequest() {

    Credentials credentials = Credentials(username: this._usernameController.text,password:this._passwordController.text);

    userSession
        .login(credentials)
        .then((loggedUser) {
          userSession.save(credentials);
          print("User logged, home will be loaded.");
          router.replaceFor(ProcedureEditorScreen.home, context);
         })
        .catchError((error) {
          print("Login failed.");
        });
  }
}