import 'package:flutter/material.dart';
import 'package:semantic_procedure_editor/classes/procedure_editor_router.dart';
import 'package:semantic_procedure_editor/screens/placeholder_widget.dart';
import 'my_procedure_list_widget.dart';
import 'my_profile_widget.dart';

/// This is the stateful widget that the main application instantiates.
class HomeWidget extends StatefulWidget {
  const HomeWidget({Key? key}) : super(key: key);

  @override
  State<HomeWidget> createState() => _HomeWidgetState();
}

/// This is the private State class that goes with MyStatefulWidget.
class _HomeWidgetState extends State<HomeWidget> {
  int _selectedIndex = 0;

  final List<Widget> _children = [
    MyProcedureListWidget(),
    PlaceholderWidget(Colors.white),
    MyProfileWidget(title: "My Profile")
  ];

  void _onItemTapped(int index, BuildContext context) {
    int indexToChange = index;

    if (index == 1) {
      presentProcedureEditor(context);
      indexToChange = 0;
    }

    setState(() {
      _selectedIndex = indexToChange;
    });
  }

  void presentProcedureEditor(BuildContext context) {
    router.presentProcedureCreationEditor(context);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: _children[_selectedIndex],
      ),
      bottomNavigationBar: BottomNavigationBar(
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.subject),
            label: 'My procedures',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.add_circle),
            label: 'Create',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.person),
            label: 'Profile',
          ),
        ],
        currentIndex: _selectedIndex,
        selectedItemColor: Colors.amber[800],
        onTap: (int index) {
          this._onItemTapped(index,context);
        },
      ),
    );
  }
}