import 'package:flutter/material.dart';
import '../blocs/user_session_bloc.dart';
import '../classes/procedure_editor_router.dart';
import '../views/procedure_editor_button.dart';
import '../views/profile_header_view.dart';

class MyProfileWidget extends StatefulWidget {
  const MyProfileWidget(
      {Key? key, required this.title})
      : super(key: key);

  final String title;

  @override
  _MyProfileWidgetState createState() => _MyProfileWidgetState();
}

class _MyProfileWidgetState extends State<MyProfileWidget> {
  final EdgeInsets containerPadding =
  EdgeInsets.only(left: 20.0, right: 20.0, bottom: 10.0);

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {

    String username = userSession.user?.username ?? "unknown";
    String profileUrl = userSession.user?.profileUrl ?? "";

    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Container(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              ProfileHeaderWidget(username: username, profileUrl: profileUrl),
              Expanded(
                  child: Align(
                    alignment: Alignment.bottomCenter,
                    child: ProcedureEditorButton(
                      buttonTitle: "Logout",
                      onPressed: () {
                        this.performLogoutRequest();
                      },
                    ),
                  ))
            ],
          ),
          padding: containerPadding
      ),
    );
  }

  void performLogoutRequest() {
    print("logout button tapped");
    userSession.logout()
               .then((value) => router.replaceFor(ProcedureEditorScreen.login, context));
  }
}