
import 'package:rxdart/rxdart.dart';
import 'package:semantic_procedure_editor/blocs/procedure_creation_editor_bloc.dart';
import 'package:semantic_procedure_editor/blocs/procedure_list_bloc.dart';
import '../api_providers/network_layer.dart';
import '../model/procedure.dart';

class ProcedureModificationEditorBLoC implements ProcedureEditorBLoC {

  final  _currentProcedure  = BehaviorSubject<Procedure?>();
  late final String _procedureLocalId;
  final  _onLoading  = BehaviorSubject<bool>();

  @override
  Procedure? get currentProcedure => _currentProcedure.valueOrNull;

  @override
  Stream<Procedure?> get onCurrentProcedureUpdates => _currentProcedure.stream;

  @override
  Stream<bool> get onLoading => _onLoading.stream;

  ProcedureModificationEditorBLoC(String procedureLocalId) {
    _procedureLocalId = procedureLocalId;
  }

  @override
  void start() {
    this._fetchProcedure(_procedureLocalId);
  }

  @override
  void dispose() {
    _currentProcedure.close();
    _onLoading.close();
  }

  void _fetchProcedure(String procedureLocalId) {
    this._onLoading.add(true);
    networkLayer.fetchProcedureDetail(procedureLocalId).then((procedure){
      this._onLoading.add(false);
      _currentProcedure.add(procedure);
    });
  }

  @override
  void performSaveRequest({required Function() onProcedureUploadFinished}) {

    Procedure? remoteProcedure = _currentProcedure.value;

    if (remoteProcedure == null) {
      return;
    }

    networkLayer
        .editProcedure(remoteProcedure)
        .then((procedure) {
      if (procedure != null) {
        procedureListBLoC.proceduresListWasUpdated();
      }
      onProcedureUploadFinished();
    });
  }
}
