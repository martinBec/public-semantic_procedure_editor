
import 'dart:async';
import 'package:rxdart/rxdart.dart';
import 'package:semantic_procedure_editor/blocs/procedure_creation_editor_bloc.dart';
import '../api_providers/network_layer.dart';
import '../model/procedure.dart';

class ProcedureListBLoC {

  final _procedureList = BehaviorSubject<List<Procedure>>();

  Stream<List<Procedure>> get onProcedureListUpdates => _procedureList.stream;
  bool _blocStarted = false;
  String _currentSubstring = "";
  int _page = 1;

  ProcedureListBLoC() {
    _procedureList.add([]);
  }

  void onStart() {
    if (_blocStarted) {
      return;
    }

    this.search("");
  }

  void search(String titleSubStr) {
    this._page = 1;
    this._currentSubstring = titleSubStr;
    this._fetchProcedureList(titleSubStr);
  }

  void nextPage() {
    this._page += 1;
    this._fetchProcedureList(this._currentSubstring);
  }

  void reload() {
    this.search(this._currentSubstring);
  }

  void _fetchProcedureList(String titleSubStr) {
    networkLayer.fetchProceduresList(titleSubStr, _page).then((procedures) {
      _blocStarted = true;

      if (_page == 1) {
        this._procedureList.add(procedures);
      }
      else {
        List<Procedure> previousProcedures = this._procedureList.value;
        previousProcedures.addAll(procedures);
        this._procedureList.add(previousProcedures);
      }
    });
  }

/*  void editProcedure(String localId, {required Function() onProcedureLoad}) {
    procedureEditorBLoC.onCurrentProcedureUpdates
                       .firstWhere((procedure) => procedure != null)
                       .then((value) => onProcedureLoad())
                       .catchError((error){
                         print(error);
                       });

    procedureEditorBLoC.startEdition(localId);
  }*/

  void dispose() {
    _procedureList.close();
  }

  void proceduresListWasUpdated() {
    this.reload();
  }

}

final procedureListBLoC = ProcedureListBLoC();