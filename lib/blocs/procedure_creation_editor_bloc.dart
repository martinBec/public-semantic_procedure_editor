import 'package:semantic_procedure_editor/api_providers/network_layer.dart';
import 'package:semantic_procedure_editor/blocs/procedure_list_bloc.dart';
import 'package:semantic_procedure_editor/model/procedure.dart';
import 'package:rxdart/rxdart.dart';
import 'package:semantic_procedure_editor/model/step.dart';

abstract class ProcedureEditorBLoC {
  Procedure? get currentProcedure;
  Stream<Procedure?> get onCurrentProcedureUpdates;
  Stream<bool> get onLoading;

  void start();
  void dispose();
  void performSaveRequest({required Function() onProcedureUploadFinished});
}

class ProcedureCreationEditorBLoC implements ProcedureEditorBLoC {

   final  _currentProcedure  = BehaviorSubject<Procedure?>();
   final  _onLoading  = BehaviorSubject<bool>();

   @override
   Stream<bool> get onLoading => _onLoading.stream;
   Procedure? get currentProcedure => _currentProcedure.valueOrNull;
   Stream<Procedure?> get onCurrentProcedureUpdates => _currentProcedure.stream;

   @override
   void start() {
     _currentProcedure.add(Procedure.draft());
   }

   @override
   void performSaveRequest({required Function() onProcedureUploadFinished}) {
       Procedure? localProcedure = this.currentProcedure;

       if (localProcedure != null) {

         List<Step> localSteps = localProcedure.steps.toList();

         networkLayer
             .uploadProcedure(localProcedure, localSteps)
             .then((procedure) {

                   if (procedure != null) {
                     procedureListBLoC.proceduresListWasUpdated();
                   }

                   onProcedureUploadFinished();
         });
       }
   }

   void dispose() {
    _currentProcedure.close();
    _onLoading.close();
   }

}
