

import 'package:semantic_procedure_editor/model/credentials.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../api_providers/network_layer.dart';
import '../model/user.dart';

class UserSessionBLoC {

  static const String USERNAME = 'username';
  static const String PASSWORD = 'password';

  User? user;

  Future<Credentials?> retrieveCredentials() async {

    final prefs = await SharedPreferences.getInstance();
    String? username = prefs.getString(USERNAME);
    String? password = prefs.getString(PASSWORD);

    Credentials? credentials;

    if (username != null && password != null) {
      credentials = Credentials(username:username , password:password);
    }

    return Future.value(credentials);
  }

  Future<User> login(Credentials credentials) {
    return networkLayer
        .login(credentials)
        .then((loggedUser) {
          this.user = loggedUser;
          return loggedUser;
        });
  }

  Future<void> save(Credentials credentials) async {
    final prefs = await SharedPreferences.getInstance();
    prefs.setString(USERNAME, credentials.username);
    prefs.setString(PASSWORD, credentials.password);
  }

  Future<void> removeCredentials() async {
    final prefs = await SharedPreferences.getInstance();
    prefs.remove(USERNAME);
    prefs.remove(PASSWORD);
  }

  Future<void> logout() {
    this.user = null;
    return this.removeCredentials();
  }
}

final userSession = UserSessionBLoC();