import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:semantic_procedure_editor/screens/initial_widget.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {

  static const String _title = 'Procedure Editor';

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      title: _title,
      home: InitialScreenWidget(),
    );
  }
}

