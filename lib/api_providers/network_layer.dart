import 'dart:collection';
import 'package:semantic_procedure_editor/model/semantic_graph.dart';
import 'package:semantic_procedure_editor/blocs/user_session_bloc.dart';
import 'package:semantic_procedure_editor/classes/proxy_step_list.dart';
import 'package:semantic_procedure_editor/model/credentials.dart';
import 'package:semantic_procedure_editor/model/procedure.dart';
import 'package:semantic_procedure_editor/model/user.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import '../classes/procedure_edition_body_builder.dart';
import '../model/step.dart';

class NetworkLayer {

  final String baseUrl = "http://192.168.0.16:8080/api/v1";

   Map<String,String> buildAuthHeader(bool authenticated) {
     Map<String,String> headers = new HashMap<String,String>();
     String? token = userSession.user?.token;

     headers["Content-Type"] = "application/json";

     if (authenticated && token != null) {
       String authorizationEntry = "Bearer " + token;
       headers["Authorization"] = authorizationEntry;
     }

    return headers;
  }

  Future<User> login(Credentials credentials) async {

    final String path = "/users/login";
    final String endpoint = this.baseUrl + path;

    Map loginDictionary = {
      'username': credentials.username,
      'password': credentials.password
    };

    //encode Map to JSON
    var loginRequestBody = json.encode(loginDictionary);
    var requestHeader = this.buildAuthHeader(false);

    final response = await http.post(Uri.parse(endpoint),body: loginRequestBody,headers: requestHeader);

    if (response.statusCode == 200) {
      // If the server did return a 200 OK response,
      // then parse the JSON.
      return User.fromJson(jsonDecode(response.body));
    } else {
      print("falle");
      // If the server did not return a 200 OK response,
      // then throw an exception.
      throw Exception('Failed to login user');
    }
  }

  Future<Procedure?> uploadProcedure(Procedure localProcedure, List<Step> localSteps) async {
     return this._uploadProcedure(localProcedure, localSteps)
                .then((procedure) => _fetchProcedureSteps(procedure));
  }

  Future<Procedure?> _uploadProcedure(Procedure localProcedure, List<Step> localSteps) async {
    final String path = "/procedures/";
    final String endpoint = this.baseUrl + path;

    Map uploadProcedureDictionary = {
      'procedure': localProcedure,
      'steps': localSteps
    };

    //encode Map to JSON
    var uploadRequestBody = json.encode(uploadProcedureDictionary);
    var requestHeader = this.buildAuthHeader(true);

    final response = await http.post(Uri.parse(endpoint),body: uploadRequestBody,headers: requestHeader);

    if (response.statusCode == 200) {
      // If the server did return a 200 OK response,
      // then parse the JSON.
      return Procedure.detail(SemanticGraph.fromString(response.body));
    } else {
      print("falle");
      // If the server did not return a 200 OK response,
      // then throw an exception.
      throw Exception('Failed to create the procedure');
    }
  }

  Future<Procedure?> fetchProcedureDetail(String procedureLocalId) async {
     return this._fetchProcedureDetail(procedureLocalId)
                .then((procedure) => _fetchProcedureSteps(procedure));
  }

  Future<Procedure?> _fetchProcedureDetail(String procedureLocalId) async {

    final String path = "/procedures/detail";
    final String endpoint = this.baseUrl + path;

    var requestHeader = this.buildAuthHeader(true);

    var queryParameters = {
      'procedure_id': procedureLocalId,
    };

    final Uri finalRequestUrl = Uri.parse(endpoint).replace(queryParameters: queryParameters);
    final response = await http.get(finalRequestUrl, headers: requestHeader);

    if (response.statusCode == 200) {
      // If the server did return a 200 OK response,
      // then parse the JSON.

      return Procedure.detail(SemanticGraph.fromString(response.body));

    } else {
      print("falle al buscar procedimientos");
      // If the server did not return a 200 OK response,
      // then throw an exception.
      throw Exception('Failed to fetch author procedures');
    }
  }

  Future<Procedure?> _fetchProcedureSteps(Procedure? procedure) async {

    final String path = "/procedures/steps/get";
    final String endpoint = this.baseUrl + path;

    var requestHeader = this.buildAuthHeader(true);

    var queryParameters = {
      'procedure_id': procedure?.id ?? "",
    };

    final Uri finalRequestUrl = Uri.parse(endpoint).replace(queryParameters: queryParameters);
    final response = await http.get(finalRequestUrl, headers: requestHeader);

    if (response.statusCode == 200) {
      // If the server did return a 200 OK response,
      // then parse the JSON.

      SemanticGraph graph = SemanticGraph.fromString(response.body);

      HashMap<String,Step> stepsMap = HashMap();

      String? firstStepLocalId = procedure?.firstStep?.id;
      Step? firstStep = procedure?.firstStep;

      if (firstStep != null && firstStepLocalId != null) {
        stepsMap[firstStepLocalId] = firstStep;
      }

      for(int i = 0; i < graph.statements.length; i++) {
        Map<String, dynamic> stepJson = graph.statements[i];
        Step step = Step(stepJson);
        stepsMap[step.id] = step;
      }

      procedure?.steps = ProxyStepList.initWithRemoteSteps(procedure.firstStep, stepsMap);

      return procedure;

    } else {
      print("falle al buscar los pasos del procedimiento");
      // If the server did not return a 200 OK response,
      // then throw an exception.
      throw Exception('Failed to fetch steps of the procedure');
    }
  }

  Future<Procedure?> editProcedure(Procedure remoteProcedure) async {
    return this._editProcedure(remoteProcedure)
        .then((procedure) => _fetchProcedureSteps(procedure));
  }

  Future<Procedure?> _editProcedure(Procedure remoteProcedure) async {
    final String path = "/procedures/";
    final String endpoint = this.baseUrl + path;

    //encode Map to JSON
    Map procedureEditionJson = ProcedureEditionBodyBuilder.build(remoteProcedure);
    var procedureEditionBody = json.encode(procedureEditionJson);
    var requestHeader = this.buildAuthHeader(true);

    final response = await http.put(Uri.parse(endpoint), body: procedureEditionBody, headers: requestHeader);

    if (response.statusCode == 200) {
      // If the server did return a 200 OK response,
      // then parse the JSON.
      return Procedure.detail(SemanticGraph.fromString(response.body));
    } else {
      print("falle");
      // If the server did not return a 200 OK response,
      // then throw an exception.
      throw Exception('Failed to edit the procedure');
    }
  }

  Future<List<Procedure>> fetchProceduresList(String titleSubstring, int page) async {

    final String path = "/author/procedures";
    final String endpoint = this.baseUrl + path;

    var requestHeader = this.buildAuthHeader(true);

    var queryParameters = {
      'page': page.toString(),
    };

    if (titleSubstring.isNotEmpty) {
      queryParameters['procedure_title'] = titleSubstring;
    }

    final Uri finalRequestUrl = Uri.parse(endpoint).replace(queryParameters: queryParameters);
    final response = await http.get(finalRequestUrl, headers: requestHeader);

    if (response.statusCode == 200) {
      // If the server did return a 200 OK response,
      // then parse the JSON.
      SemanticGraph graph = SemanticGraph.fromString(response.body);

      return  graph.statements
                   .map((statement) => Procedure.listItem(statement))
                   .toList();
    } else {
      print("falle al buscar procedimientos");
      // If the server did not return a 200 OK response,
      // then throw an exception.
      throw Exception('Failed to fetch author procedures');
    }
  }
}

NetworkLayer networkLayer = NetworkLayer();